// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import VueCodeMirror from 'vue-codemirror'
//import vSelect from 'vue-select'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-default/index.css'



Vue.use(VueCodeMirror)
//Vue.component('v-select', vSelect)
Vue.config.productionTip = false
Vue.use(ElementUI)

import App from './App'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  template: '<App/>',
  components: { App }
})
