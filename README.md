# sense-helper (working title)


## Roadmap 

#### v1.0 

* design 
    * not completely finished. experimenting
    * section operations should work - add, delete, move up/down etc

* functionality
    * the only possible result will be in `textbox` style - result from only one expression will be returned
    * history - save to local (browser) storage should be implemented


#### v2.0 

* design 
    * more polished - clear what the final layout will/should be

* functionality
    * investigate if QS session objects can be passed as result
    * if QS session objects can't be used - decide what to do
    * add dimensions to the result
    
#### v3.0 

* design 
    * finished

* functionality
    * charts as a result    
    * multiple expressions
    
#### v4.0

* functionality - QS Server

#### v6.0+

* functionality - Script notebooks
* Feedback